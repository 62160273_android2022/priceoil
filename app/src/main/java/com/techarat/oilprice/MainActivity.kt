package com.techarat.oilprice

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.techarat.oilprice.model.Oil

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val PriceOil : List<Oil> = listOf(
            Oil(36.84,"เชลล์ ฟิวเซฟ แก๊สโซฮอล์ E20"),
            Oil(37.68,"เชลล์ ฟิวเซฟ แก๊สโซฮอล์ 91" ),
            Oil(37.95,"เชลล์ ฟิวเซฟ แก๊สโซฮอล์ 95" ),
            Oil(45.44,"เชลล์ วีพาวเวอร์ แก๊สโซฮอล์ 95" ),
            Oil(36.34,"เชลล์ ดีเซล B20" ),
            Oil(36.34,"เชลล์ ฟิวเซฟ ดีเซล"  ),
            Oil(36.34,"เชลล์ ฟิวเซฟ ดีเซล B7"  ),
            Oil(36.34,"เชลล์ วีพาวเวอร์ ดีเซล" ),
            Oil(47.06,"เชลล์ วีพาวเวอร์ ดีเซล B7" )
        )

        val recyclerView = findViewById<RecyclerView>(R.id.recyclerView)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = ItemAdapter(PriceOil)
        var adapters = ItemAdapter(PriceOil)
        recyclerView.adapter = adapters
        adapters.setOnItemClickListener(object : ItemAdapter.onItemClickListener {
            override fun onItemClick(position: Int) {
                Toast.makeText(
                    this@MainActivity,
                    PriceOil[position].Price.toString() + "     " + PriceOil[position].NameOil,
                    Toast.LENGTH_LONG
                ).show()
            }
        })
    }

}
    class ItemAdapter(val Oil : List<Oil>) : RecyclerView.Adapter<ItemAdapter.ViewHolder>() {

        private lateinit var mListener: onItemClickListener

        interface onItemClickListener {
            fun onItemClick(position: Int)
        }

        fun setOnItemClickListener(listener: onItemClickListener) {
            mListener = listener
        }

        class ViewHolder(private val itemView: View, listener: onItemClickListener) : RecyclerView.ViewHolder(itemView){
            val NameOil = itemView.findViewById<TextView>(R.id.NameOil)
            val Price = itemView.findViewById<TextView>(R.id.Price)
            init {
                itemView.setOnClickListener {
                    listener.onItemClick(adapterPosition)
                }
            }
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val adapterLayout = LayoutInflater.from(parent.context).inflate(R.layout.item , parent ,false)
            return ViewHolder(adapterLayout,mListener)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.NameOil.text = Oil[position].NameOil
            holder.Price.text = Oil[position].Price.toString()
        }

        override fun getItemCount(): Int {
            return Oil.size
        }
    }
    }